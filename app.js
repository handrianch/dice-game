/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, activePlayer, currentScore, isGamePlay, finalScore, input, lastDice;

init();

document.querySelector(".btn-roll").addEventListener("click", () => {
    if(isGamePlay){
        var dice = [getRandomNumber(), getRandomNumber()];

        for (var i = 0; i < dice.length; i++) {
            document.querySelectorAll(".dice")[i].style.display = "block";
            document.querySelectorAll(".dice")[i].src = `dice-${dice[i]}.png`;
            dice[i] === 6 ? lastDice[i]++ : lastDice[i] = 0;
        }

        if(lastDice[0] === 2 && lastDice[1] === 2){
            scores[activePlayer] = 0;
            document.querySelector(`#score-${activePlayer}`).textContent = scores[activePlayer];
            nextPlayer();
        } else if (dice[0] !== 1 && dice[1] !== 1) {
            currentScore += dice[0] + dice[1];
            document.querySelector(`#current-${activePlayer}`).textContent = currentScore;
        } else {
            nextPlayer();
        }
    }
});

document.querySelector(".btn-hold").addEventListener("click", () => {
    if(isGamePlay){
        scores[activePlayer] += currentScore;
        document.querySelector(`#score-${activePlayer}`).textContent = scores[activePlayer];

        if (scores[activePlayer] >= finalScore) {
            document.querySelector(`#name-${activePlayer}`).textContent = "Winner!";
            document.querySelector(`.player-${activePlayer}-panel`).classList.add("winner");
            document.querySelector(`.player-${activePlayer}-panel`).classList.remove("active");
            isGamePlay = false;
            document.querySelector(".final-score").disabled = false;
            for(var i = 0; i < scores.length; i++){
                document.querySelectorAll(".dice")[i].style.display = "none";
            }
        } else {
            nextPlayer();
        }
    }
});

document.querySelector(".btn-new").addEventListener("click", () => {
    init();
    document.querySelector(".final-score").disabled = true;
});

function init() {
    scores = [0, 0];
    activePlayer = 0;
    currentScore = 0;
    isGamePlay = true;
    lastDice = [0,0];

    for (var i = 0; i < scores.length; i++) {
        document.querySelector(`#score-${i}`).textContent = 0;
        document.querySelector(`#current-${i}`).textContent = 0;
        document.querySelector(`#name-${i}`).textContent = `Player ${i + 1}`;
        // document.querySelector
        document.querySelectorAll(".dice")[i].style.display = "none";
        document.querySelector(`.player-${i}-panel`).classList.remove("active");
        document.querySelector(`.player-${i}-panel`).classList.remove("winner");

    }
    document.querySelector(`.player-0-panel`).classList.add("active");

    lastDice = [0, 0];

    input = document.querySelector(".final-score").value;

    if (Number(input)) {
        finalScore = Number(input);
    } else {
        finalScore = 100;
    }
}

function getRandomNumber(){
    return Math.floor(Math.random() * 6) + 1;
}

function nextPlayer(){
    lastDice = [0,0];
    currentScore = 0;
    document.querySelector(`#current-${activePlayer}`).textContent = 0;
    activePlayer === 1 ? activePlayer = 0 : activePlayer = 1;

    for (var i = 0; i < scores.length; i++) {
        document.querySelector(`.player-${i}-panel`).classList.toggle("active");
        // document.querySelector(`#dice-${i+1}`).style.display = "none";
    }
}